import { ControlcenterNgPage } from './app.po';

describe('controlcenter-ng App', () => {
  let page: ControlcenterNgPage;

  beforeEach(() => {
    page = new ControlcenterNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
