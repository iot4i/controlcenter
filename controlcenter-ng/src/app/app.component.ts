import {Component} from '@angular/core';
import {Http} from '@angular/http';
import {ColorService} from './services/color.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ColorService]
})
export class AppComponent {
  constructor(private http: Http, private colorService: ColorService) {
  }
  mainColorChanged(color) {
    this.colorService.update(color).subscribe(r => console.log('Color updated'));
  };
}
