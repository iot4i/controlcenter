import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from './app.component';
import {ColorPanelComponent} from './components/colorpanel/colorpanel.component';
import {ColorPickerDirective, ColorPickerService} from 'ngx-color-picker';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, ColorPanelComponent, ColorPickerDirective
      ], providers: [ColorPickerService]
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
