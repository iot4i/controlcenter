import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {ColorService} from '../../services/color.service';
import {Color} from '../../model/Color';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/skip';
import 'rxjs/add/operator/distinctUntilChanged';
import {Subject} from 'rxjs/Subject';
import {IntervalObservable} from 'rxjs/observable/IntervalObservable';

@Component({
  selector: 'cc-colorpanel',
  templateUrl: './colorpanel.component.html',
  styleUrls: ['./colorpanel.component.css'],
  providers: [ColorService]
})

export class ColorPanelComponent implements OnInit {

  private color: Color = new Color('#000000');
  private colorStream = new Subject<String>();
  @Output() colorChanged = new EventEmitter();

  constructor(private colorService: ColorService) {
    // Subscribe to debounce output events
    // I would love to Output directly the observable
    this.colorStream.asObservable().debounceTime(100)
      .distinctUntilChanged().skip(1).subscribe(e => this.colorChanged.emit(new Color(e)));
    // Subscribe internal for changing the color
    this.colorStream.subscribe(e => this.color = new Color(e));
    IntervalObservable.create(1000)
      .subscribe(() => this.colorService.current().subscribe(color => this.color = color, e => console.log(e)));
  }

  ngOnInit() {
    this.colorService.current().subscribe(color => this.color = color, e => console.log(e));
  }

}
