import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ColorPanelComponent} from './colorpanel.component';
import {ColorPickerDirective, ColorPickerService} from 'ngx-color-picker';

describe('ColorPanelComponent', () => {
  let component: ColorPanelComponent;
  let fixture: ComponentFixture<ColorPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ColorPanelComponent, ColorPickerDirective],
      providers: [ColorPickerService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ColorPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should contains a color picker', async(() => {
    expect(component).toBeTruthy();
    expect(fixture.debugElement.nativeElement.querySelector('div').textContent).toContain('Choisir une couleur');
  }));
});
