import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Color} from 'app/model/Color';
import {environment} from 'environments/environment';

import 'rxjs/add/operator/map';

@Injectable()
export class ColorService {

  constructor(private http: Http) {
  }

  public current(): Observable<Color> {
    return this.http.get(environment.apiBaseUrl + '/api/v1/color').map(s => s.json());
  }

  public update(color: Color): Observable<Color> {
    return this.http.patch(environment.apiBaseUrl + '/api/v1/color', color).map(s => s.json());
  }

}
