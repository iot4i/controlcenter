package io.vertx.scala.sbt

import io.vertx.core.http.HttpMethod
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.lang.scala.json.{Json, JsonObject}
import io.vertx.scala.core.shareddata.LocalMap
import io.vertx.scala.ext.web.Router
import io.vertx.scala.ext.web.handler.{BodyHandler, CorsHandler, ResponseContentTypeHandler}

import scala.concurrent.Future

class HttpVerticle extends ScalaVerticle {


  override def startFuture(): Future[Unit] = {
    val router = Router.router(vertx)

    /* Cors - to remove */
    router.route().handler(CorsHandler.create("*").allowedMethod(HttpMethod.GET).allowedMethod(HttpMethod.PATCH).allowedHeader("Content-Type"))
    /* Manage content type on responses */
    router.route().handler(ResponseContentTypeHandler.create())
    /* Manage body */
    router.route().handler(BodyHandler.create())

    /* The local cache, will be replaced by a database */
    val colorMap: LocalMap[String, String] = vertx.sharedData().getLocalMap("color")
    colorMap.put("hexa", "#000000")

    /* GET handling on color endpoint */
    router
      .get("/api/v1/color")
      .produces("application/json")
      .handler(_.response().end(Json.obj("hexa" -> colorMap.get("hexa")).encode()))
    /* PATCH handling on color endpoint */
    router
      .patch("/api/v1/color")
      .produces("application/json")
      .handler((rc: io.vertx.scala.ext.web.RoutingContext) => {
        rc.getBodyAsJson().fold(rc.response().setStatusCode(400).end()) {
          color: JsonObject => {
            colorMap.put("hexa", color.getString("hexa"))
            rc.response().end(Json.obj("hexa" -> color.getString("hexa")).encode())
          }
        }
      })

    /* Starting the http server */
    vertx
      .createHttpServer()
      .requestHandler(router.accept)
      .listenFuture(9000, "0.0.0.0")
      .map(_ => ())
  }
}
