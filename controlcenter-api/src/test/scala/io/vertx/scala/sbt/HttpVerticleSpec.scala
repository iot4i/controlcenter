package io.vertx.scala.sbt

import io.vertx.core.http.HttpMethod
import io.vertx.core.http.HttpMethod.{GET, PATCH}
import io.vertx.lang.scala.json.Json
import org.scalatest.Matchers

import scala.concurrent.Promise

class HttpVerticleSpec extends VerticleTesting[HttpVerticle] with Matchers {

  def theCallTo(method: HttpMethod, path: String, withBody: String = ""): Promise[String] = {
    val promise = Promise[String]
    vertx.createHttpClient()
      .request(method, 9000, "127.0.0.1", path,
        r => {
          r.exceptionHandler(promise.failure)
          r.bodyHandler(b => promise.success(b.toString))
        }).end(withBody)
    return promise
  }

  def aJsonWithHexaValue(hexa: String) = Json.obj("hexa" -> hexa).encode()

  "HttpVerticle" should "bind to port 9000 and answer with default color on /api/v1/color/" in {
    theCallTo(GET, "/api/v1/color")
      .future.map(res => res should equal(aJsonWithHexaValue("#000000")))
  }
  it should "accept to patch the current color on /api/v1/color/" in {
    theCallTo(PATCH, "/api/v1/color", aJsonWithHexaValue("#FF0000"))
      .future.map(res => res should equal(aJsonWithHexaValue("#FF0000")))
  }
  it should "return the patched color on /api/v1/color/" in {
    theCallTo(PATCH, "/api/v1/color", aJsonWithHexaValue("#FFFF00"))
      .future.flatMap(_ =>
      theCallTo(GET, "/api/v1/color")
        .future.map(res => res should equal(aJsonWithHexaValue("#FFFF00"))))
  }

}
