iot4i/controlcenter
=================
![controlcenter-banner](/uploads/6c51344fc0fbb26616b6a15b33ec62e5/controlcenter-banner.jpg)


The control center for the **iot** devices.

This application will manage the different peripherals and permit the control of them with a RESTful api.

This application contains the RESTful api server (**controlcenter-api**) and the angular 4 client (**controlcenter-ng**)

### Prerequisites
You need docker and docker-compose installed on your environment.

## To build and run the whole application
```bash
docker-compose -f docker-compose-build.yml up --build
```

To run the last production ready application: 
```bash
docker-compose -f docker-compose.yml up
```

# 1. controlcenter-api

This application is developed in Scala, with Eclipse Vert.x and sbt.

### Prerequisites

 If you want to build without docker, you will need openjdk and sbt installed.

### Building

#### With docker

To compile, test, package :
```
docker build -t digitallumberjack/controlcenter-api:dev .
```

The first time you build the image, sbt will load all dependencies in a layer that is cached by docker. Until you do not modify `build.sbt` or `project/*` files, the build will be really fast.

### Running

From the previously built docker image:
```
docker run --rm -ti -p 9000:9000 digitallumberjack/controlcenter-api:dev
```

#### Running directly from sbt

```
sbt
> console
scala> vertx.deployVerticle(nameForVerticle[HttpVerticle])
scala> vertx.deploymentIDs
```

From here you can freely interact with the Vertx-API inside the sbt-scala-shell.


# 2. controlcenter-ng

### Prerequisites

 If you want to build without docker, you will need node >=7.6.0, ng-cli >= 1.0.0 and npm >=4.2.0

### Building

#### With docker

To compile, test, package :
```
docker build -t digitallumberjack/controlcenter-ng:dev .
```

### Running

From the previously built docker image:
```
docker run --rm -ti -p 4200:4200 digitallumberjack/controlcenter-ng:dev
```

#### Running directly from ng-cli

```
ng-serve --host 0.0.0.0 --port 4200
```

## Contributing

Please use .editorconfig formatting options from each directory.

Use Issues and related Merge Requests to contribute.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](/tags). 

## Authors

* **[digitalLumberjack](https://digitalLumberjack.me)**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
